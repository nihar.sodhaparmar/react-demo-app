import styled from "styled-components";
import { colors, breakpoints, SITE_WIDTH } from "./theme";

export const Container = styled.div`
	max-width: ${SITE_WIDTH};
	width: 100%;
	padding-right: 15px;
	padding-left: 15px;
	margin-right: auto;
	margin-left: auto;

	@media screen and (max-width: ${breakpoints.tablet}px) {
		max-width: 720px;
	}
`;

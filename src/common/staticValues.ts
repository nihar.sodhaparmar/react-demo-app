export const headerLinks = [
	{ name: "home", link: "/" },
	{ name: "about us", link: "/about" },
	{ name: "causes", link: "/causes" },
	{ name: "gallery", link: "/gallery" },
	{ name: "news", link: "/news" },
	{ name: "contact", link: "/contact" },
];

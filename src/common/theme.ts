const colors = {
	primary: "#ff4800",
	white: "#ffffff",
	black: "#262626",
	lightGrey: "#ecf2f5",
	lightBlack: "#595858",
	grey: "#b7b7b7",
};

const breakpoints = {
	desktop: 1400,
	laptop: 1024,
	tablet: 768,
	mobile: 576,
};

const SITE_WIDTH = "1140px";

export { colors, breakpoints, SITE_WIDTH };

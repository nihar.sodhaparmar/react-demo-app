import React from "react";
import styled from "styled-components";
import { Container } from "../../common/style";
import { colors } from "../../common/theme";
import Header from "../../widgets/Header";
import TopHeaderBar from "../../widgets/TopHeaderBar";

const MessageWrapper = styled.div`
	color: ${colors.black};
	min-height: 60vh;
	display: flex;
	align-items: center;
	justify-content: center;
`;

export default function PageNotFound() {
	return (
		<>
			<TopHeaderBar />
			<Header selectedLink={"contact"} />
			<Container>
				<MessageWrapper>
					<h1>Page Not Found</h1>
				</MessageWrapper>
			</Container>
		</>
	);
}

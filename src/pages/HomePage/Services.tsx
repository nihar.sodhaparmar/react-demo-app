import React from "react";
import styled from "styled-components";
import { Container } from "../../common/style";
import { breakpoints, colors } from "../../common/theme";

const Wrapper = styled.div`
	padding: 96px 0;
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
	width: 100%;

	@media screen and (max-width: ${breakpoints.tablet}px) {
		padding: 50px 0;
	}

	@media screen and (max-width: ${breakpoints.mobile}px) {
		padding: 30px 0;
	}
`;

const Service = styled.div<any>`
	margin: 12px 0;
	width: 31.5%;
	background-color: ${colors.lightGrey};
	border-radius: 24px;
	padding: 40px;
	* {
		color: ${colors.black};
	}
	.image {
		display: block;
	}
	.hovered-image {
		display: none;
	}

	:hover {
		background-color: ${colors.primary};
		.hovered-image {
			display: block;
		}
		.image {
			display: none;
		}
		* {
			color: ${colors.white};
		}
	}

	@media screen and (max-width: ${breakpoints.tablet}px) {
		width: 48%;
	}

	@media screen and (max-width: ${breakpoints.mobile}px) {
		width: 100%;
	}
`;

const StyledImage = styled.img`
	margin: 0 auto;
`;

const Header = styled.h3`
	text-align: center;
	margin: 28px 0 22px;
	font-size: 24px;
`;

const Content = styled.div`
	text-align: center;
	font-size: 14px;
	color: ${colors.lightBlack};
`;

export default function Services() {
	return (
		<div>
			<Container>
				<Wrapper>
					<Service>
						<StyledImage
							className="image"
							src="https://preview.colorlib.com/theme/thecharity/images/xhands-gray.png.pagespeed.ic.P-Favi4yos.webp"
							alt="volunteer"
						/>
						<StyledImage
							className="hovered-image"
							src="https://preview.colorlib.com/theme/thecharity/images/xhands-white.png.pagespeed.ic.e2aNYXAbJk.webp"
							alt="volunteer"
						/>
						<Header>Become a Volunteer</Header>
						<Content>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris quis aliquam.
						</Content>
					</Service>
					<Service>
						<StyledImage
							className="image"
							src="https://preview.colorlib.com/theme/thecharity/images/xdonation-gray.png.pagespeed.ic.V5mawri7ez.webp"
							alt="volunteer"
						/>
						<StyledImage
							className="hovered-image"
							src="https://preview.colorlib.com/theme/thecharity/images/xdonation-white.png.pagespeed.ic.mE7YjoKuwn.webp"
							alt="volunteer"
						/>
						<Header>Dance & Music</Header>
						<Content>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris quis aliquam.
						</Content>
					</Service>
					<Service>
						<StyledImage
							className="image"
							src="https://preview.colorlib.com/theme/thecharity/images/xcharity-gray.png.pagespeed.ic.wZh4Gb9lD9.webp"
							alt="volunteer"
						/>
						<StyledImage
							className="hovered-image"
							src="https://preview.colorlib.com/theme/thecharity/images/xcharity-white.png.pagespeed.ic.9xAPMbcTQ5.webp"
							alt="volunteer"
						/>
						<Header>Online Conference</Header>
						<Content>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris quis aliquam.
						</Content>
					</Service>
				</Wrapper>
			</Container>
		</div>
	);
}

import React from "react";
import styled from "styled-components";
import { Container } from "../../common/style";
import { breakpoints, colors } from "../../common/theme";

const Wrapper = styled.div`
	background-image: url("https://preview.colorlib.com/theme/thecharity/images/xwelcome-bg.jpg.pagespeed.ic.7Z3ys5K1YE.jpg");
	width: 100%;
	background: rgba(21, 21, 21, 0.9);
`;

const InnerContainer = styled.div`
	display: flex;
	justify-content: space-between;
	padding: 80px 0;

	@media screen and (max-width: ${breakpoints.tablet}px) {
		flex-direction: column-reverse;
	}
`;

const ImageWrapper = styled.div`
	width: 50%;
	@media screen and (max-width: ${breakpoints.tablet}px) {
		width: 100%;
	}
`;

const StyledImage = styled.img`
	display: block;
	padding-left: 20px;
	width: 100%;
	height: 100%;
	@media screen and (max-width: ${breakpoints.tablet}px) {
		padding: 0;
	}
`;

const Header = styled.h2`
	font-size: 36px;
	padding-bottom: 24px;
	font-weight: 600;
	color: ${colors.white};
	margin: 0;
`;

const UnderLine = styled.div`
	border: 3px solid ${colors.primary};
	border-radius: 3px;
	width: 80px;
`;

const ContentWrapper = styled.div`
	width: 50%;
	@media screen and (max-width: ${breakpoints.tablet}px) {
		width: 100%;
		margin-top: 30px;
	}
`;

const Content = styled.p`
	margin: 40px 0;
	color: ${colors.grey};
	font-size: 14px;
	line-height: 2;
`;

const Button = styled.button`
	background-color: ${colors.primary};
	color: ${colors.white};
	border: 0;
	border-radius: 24px;
	padding: 18px 40px;
`;

export default function Introduction() {
	return (
		<Wrapper>
			<Container>
				<InnerContainer>
					<ContentWrapper>
						<Header>Welcome to our Charity</Header>
						<UnderLine />
						<Content>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris quis aliquam.
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestibulum mauris quis aliquam.
							Integer accumsan sodales odio, id tempus velit ullamcorper id. Quisque at erat eu libero consequat tempus.
							Quisque molestie convallis tempus. Ut semper purus metus, a euismod sapien sodales ac. Duis viverra
							eleifend fermentum.
						</Content>
						<Button type="button">Read More</Button>
					</ContentWrapper>
					<ImageWrapper>
						<StyledImage
							src="https://preview.colorlib.com/theme/thecharity/images/xwelcome.jpg.pagespeed.ic.ZxwI8hfD8G.webp"
							alt="introduction logo"
						/>
					</ImageWrapper>
				</InnerContainer>
			</Container>
		</Wrapper>
	);
}

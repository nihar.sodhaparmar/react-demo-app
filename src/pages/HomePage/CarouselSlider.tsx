import React from "react";
import styled from "styled-components";
import { breakpoints, colors } from "../../common/theme";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const SliderWrapper = styled.div`
	width: 100%;
`;

const SlideImage = styled.img`
	width: 100%;
`;

const CustomSlider = styled(Slider)`
	.slick-slide {
		padding: 0;
	}

	.slick-slide img {
		height: auto;
	}

	/* Next and Prev buttons */
	button.slick-arrow {
		position: absolute;
		border: 0;
		border-radius: 50%;
		width: 70px;
		height: 70px;
		background-color: ${colors.primary};
		font-size: 0;
		top: 50%;
		transform: translateY(-50%);
		z-index: 1;
		color: ${colors.white};
	}
	.slick-prev {
		left: 80px;
	}
	.slick-next {
		right: 80px;
	}
	.slick-next:before,
	.slick-prev:before {
		color: ${colors.white};
		font-family: "FontAwesome";
		font-size: 35px;
		opacity: 1;
	}
	.slick-prev:before {
		content: "\f104";
	}
	.slick-next:before {
		content: "\f105";
	}

	/* dots */
	ul.slick-dots {
		bottom: 25px;
		@media screen and (max-width: ${breakpoints.tablet}px) {
			display: none !important;
		}
	}
	.slick-dots li button {
		padding: 0;
	}
	.slick-dots li button:focus:before {
		opacity: 0.25;
	}
	.slick-dots li button::before {
		font-size: 14px;
		line-height: 20px;
		color: ${colors.primary};
	}
	.slick-dots li.slick-active button::before {
		opacity: 1;
		color: ${colors.primary};
	}
	.slick-dots li.slick button::before {
		opacity: 0.75;
		color: ${colors.primary};
	}

	.slick-dots li button:hover {
		color: ${colors.primary};
	}

	@media screen and (max-width: ${breakpoints.tablet}px) {
		button.slick-arrow {
			display: none !important;
		}
	}
`;

const slickSliderSettings = {
	dots: true,
	arrows: true,
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 5000, // 5 seconds
	pauseOnHover: true,
	pauseOnDotsHover: true,
};

interface ICarouselSlider {
	images: Array<string>;
}

export default function CarouselSlider(props: ICarouselSlider) {
	const { images } = props;

	return (
		<SliderWrapper>
			<CustomSlider {...slickSliderSettings}>
				{images.map((url, index) => (
					<SlideImage src={url} alt={url} key={index} />
				))}
			</CustomSlider>
		</SliderWrapper>
	);
}

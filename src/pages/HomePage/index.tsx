import React from "react";
import Header from "../../widgets/Header";
import TopHeaderBar from "../../widgets/TopHeaderBar";
import CarouselSlider from "./CarouselSlider";
import Introduction from "./Introduction";
import Services from "./Services";

const ImageUrls = [
	"https://preview.colorlib.com/theme/thecharity/images/xhero.jpg.pagespeed.ic.AOvHHXG-b_.webp",
	"https://preview.colorlib.com/theme/thecharity/images/xhero.jpg.pagespeed.ic.AOvHHXG-b_.webp",
	"https://preview.colorlib.com/theme/thecharity/images/xhero.jpg.pagespeed.ic.AOvHHXG-b_.webp",
];

export default function HomePage() {
	return (
		<>
			<TopHeaderBar />
			<Header selectedLink={"home"} />
			<CarouselSlider images={ImageUrls} />
			<Services />
			<Introduction />
		</>
	);
}

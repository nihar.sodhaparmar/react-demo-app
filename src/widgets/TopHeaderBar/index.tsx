import React from "react";
import styled from "styled-components";
import { Container } from "../../common/style";
import { breakpoints, colors } from "../../common/theme";

export const TopHeaderBarWrapper = styled.div`
	background-color: ${colors.primary};
	color: ${colors.white};
	line-height: 1;
	font-size: 12px;
	height: fit-content;
`;

const TopHeaderBarInnerContainer = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;

	@media screen and (max-width: ${breakpoints.tablet}px) {
		flex-direction: column;
	}
`;

const TopHeaderBarContactContainer = styled.div`
	padding: 20px 0;
	display: flex;

	@media screen and (max-width: ${breakpoints.mobile}px) {
		display: none;
	}

	@media screen and (max-width: ${breakpoints.tablet}px) {
		margin-bottom: 1rem;
	}
`;

const TopHeaderBarPhone = styled.div`
	margin-left: 48px;
`;

const DonateButtonContainer = styled.div`
	display: block;
`;

const DonateButton = styled.a`
	background-color: ${colors.black};
	font-size: 14px;
	line-height: 1;
	font-weight: 500;
	display: inline-block;
	padding: 20px 40px;
`;

export default function TopHeaderBar() {
	return (
		<TopHeaderBarWrapper>
			<Container>
				<TopHeaderBarInnerContainer>
					<TopHeaderBarContactContainer>
						<div>MAIL: contact@ourcharity.com</div>
						<TopHeaderBarPhone>PHONE: +24 3772 120 091 / +56452 4567</TopHeaderBarPhone>
					</TopHeaderBarContactContainer>
					<DonateButtonContainer>
						<DonateButton>Donate Now</DonateButton>
					</DonateButtonContainer>
				</TopHeaderBarInnerContainer>
			</Container>
		</TopHeaderBarWrapper>
	);
}

import React, { useState } from "react";
import styled, { css } from "styled-components";
import { headerLinks } from "../../common/staticValues";
import { Container } from "../../common/style";
import { breakpoints, colors } from "../../common/theme";
import HamBurger from "./HamBurger";

const Wrapper = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;

	@media screen and (max-width: ${breakpoints.tablet}px) {
		flex-wrap: wrap;
	}
`;

const StyledImage = styled.img`
	display: block;
	margin-top: 6px;
`;

const ImageContainer = styled.div`
	padding: 20px 0;
`;

const HeaderUl = styled.ul<{ open: boolean }>`
	list-style-type: none;
	display: flex;
	padding: 0;
	margin: 0;

	@media screen and (max-width: ${breakpoints.tablet}px) {
		${({ open }) =>
			open
				? css`
						width: 100%;
				  `
				: css`
						display: none;
				  `}
		width: 100%;

		flex-direction: column;
	}
`;

const HeaderLink = styled.a<{ isCurrent?: boolean }>`
	display: block;
	padding: 49px 0 30px;
	margin: 0 16px;
	font-size: 14px;
	font-weight: 600;
	line-height: 1;
	color: ${colors.black};
	text-decoration: none;
	transition: color 0.35s;
	text-transform: capitalize;

	:hover {
		border-bottom: 3px solid #ff5a00;
		color: #ff5a00;
	}

	${(p) =>
		p.isCurrent &&
		css`
			border-bottom: 3px solid #ff5a00;
			color: #ff5a00;
		`}

	@media screen and (max-width: ${breakpoints.tablet}px) {
		padding: 20px 0;
	}
`;

const HamBurgerWrapper = styled.div`
	display: none;

	@media screen and (max-width: ${breakpoints.tablet}px) {
		display: block;
	}
`;

interface IHeaderProps {
	selectedLink: string;
}

export default function Header(props: IHeaderProps) {
	const [open, setOpen] = useState(false);
	const { selectedLink } = props;

	return (
		<>
			<Container>
				<Wrapper>
					<ImageContainer>
						<StyledImage
							src="data:image/webp;base64,UklGRvQFAABXRUJQVlA4TOgFAAAvxgAHEF4RCCTn7z1DRExAuSPIkW1btZVe7S84fHeXg+dOSOpuIU1wdyIid3d3d2c9Ofc+rRuves2gAXThRXgXfkQIMVWrAQT9h6pt+/rkmQ8ncZGWlqlwDByAS9JK4xg87lrDJY7mmtzdLTGa+yMFgAACcGzbn9lt22q2bdtWR7NtG7Nt2zsGbhsp8jIdwx+kaNsbR9JtfCjd/yaWZDvpZ/CPei9UyEK+QFChmi2DqtegWps1Eputwgrd5H9MENK2GoYUUvgKKaSQQgopTGEKX2EKKXyFOZAkNW7Gty2lFoQMzhMYhYFy6lGfuriqS3CDf5r6UkyVmVHdsdKvZta6kGeolFBeymkZX/vxvT9XBlI5GE3/KJqQbyGljbkO7nRyu5u5HpJ7kQ49sGBMySW43dLc4uKtP6g1M+p0oM5wMd6XQCmUQT/oDwNhMF+GEkaSDQQ28aYF2qADOqEbeqCXZw7rfNEomVx2VPe/W5hZtjjD2vhl4BsMQ6GCNLKOhAb+N4GvFxy/HOZ5gTKKpi9PlCv0cLsUQ8x41Beigr9VWNZgVcffBghy4HjoEA8eW9CDzZbosuDlEjpjfOK93J13av0AAOUYUX1hKqCK8xou6yDMJ0dUykMMcpqmYHENnfHV1Ok0m1GDXqHcnZIYCyoyRBlj4aqgBsI5prKgRim/qqIsaDX4YqzXDZdBpSVc1Z6gQMGZZ5eaqIEHzLvia2tYzkZRasC1KYsPxscC4HoWP1JoK+TANK/fODiNKyLI8Shj64ucomsuZssnbYx5piM2ysg0DBbaqnPZ+KpKmMuYR3MZzWwnDIk2NONnCjOrgEjzqgj6m7GL4q0V01KnKaGEQrT0Oo8nUxZ2ujR1+ZA8WQH4KgUCT2aUKDPeBCzt4Pwey3tJZrszPH5iT4bhxQQ5PmdsmoJTaw0qCtgACGYUEDxVSNkKRyEPsyd3/yrcw7xFyd35pFtIzGzwJkg2oYIHmBikGcZGwK1b9O6b2hwS3fVBNqd4+syTLOt6GJfE59jI3IbNwyjhU0jZQEq58Cibs1Ccxn5mBIBrMaLXcb1miLyAPISEUYqnACM8YHQZO0ZZ1kltLgnz6pGkp1nGJ/E4wjJWocTwbyAI8XMQAXHGwN6FeVBihgM7Zrq+zCK+56juGiW4NGrBQs0QNY49g6OcXEfaNkAFFJri3LklSbcuGFtEgmPGIZixILSkvEo9ZyFSslPeNxTlA3z4wsSiMKCIkZLZE7vmogxN40rStCYkaUEz3aVsTOGlfVN8/OfYHHGOdYc8s/O/KmBbdNsTiCIjg5ZNIl4GGVOGojGccGYbSXr3xbtvdGYNSVrawINHjs8zMvanlzqHBHPtEdesWzewrVuUVwp2WqSVEFpR/Gxtmo//vP1CknYOM7Gcz69ekHRxG+n6cQ+ZfeptIJGvimGeeHoXt7NAtWo3LtVZDd0+/j958/FjXvvPV6942xmVoA6OryAvwO3sY01ugpYQurRE42vPpmnJ52vWCAParM4XWAV7oMq6tIkkHVvl4Cw9JfxLNj/+f4DLsANG5QCTsjrzbpdzTi3XdtZKcOHwLBiFGtaL4i71JQdzk6ZWUgXFKRVIEiwimQM87d7UWmNBdzmnK+sdq6wLLmxgibiUaMZXd4r80WAmkoaywOuStKqfQnHXJIJ/pd85gk+19c0m/zun3ybOlQkV12bJVAJNvRfxxuX3wqgEH37/CzbD14LeuZEoSVfrvK9OZnUe109C2lzaBEIfRSk7mbahMfwIv8EswdUbF1HVwccruj4J3oIScE8wOTcyPhBSpf/OP4B8AJZ15w8UoZTgCxfx+5FOiL8E+6EM/AIPWIXT1eMpwT7CRsGnvxiRYZnxVw38BQcIWwRtCQcFa/gFPhFMYUQGisnWJHwD5wj14B84TzgkmENCP8FnUJKRF6ik6P41p1aE9+A+WYT/oTEcEYwhCScFMxh1YRQF7C5ZY8HI/yTUhfqEY/A99CX5B16DaYywkHjjU4Ev/dKezGxQzgE="
							alt="logo"
						/>
					</ImageContainer>
					<HamBurgerWrapper>
						<HamBurger open={open} setOpen={setOpen} />
					</HamBurgerWrapper>
					<HeaderUl open={open}>
						{headerLinks.map((link, index) => {
							return (
								<li key={index}>
									<HeaderLink href={link.link} isCurrent={link.name === selectedLink}>
										{link.name}
									</HeaderLink>
								</li>
							);
						})}
					</HeaderUl>
				</Wrapper>
			</Container>
		</>
	);
}

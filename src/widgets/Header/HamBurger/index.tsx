import React, { Dispatch, SetStateAction } from "react";
import styled from "styled-components";
import { colors } from "../../../common/theme";

const StyledBurger = styled.button<{ open: boolean }>`
	padding: 0;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
	width: 1.5rem;
	height: 1.5rem;
	background: transparent;
	border: none;
	cursor: pointer;

	&:focus {
		outline: none;
	}

	div {
		width: 1.5rem;
		height: 0.125rem;
		transition: all 0.3s linear;
		position: relative;
		transform-origin: 1px;
		background-color: ${colors.black};

		:first-child {
			transform: ${({ open }) => (open ? "rotate(45deg)" : "rotate(0)")};
		}

		:nth-child(2) {
			opacity: ${({ open }) => (open ? "0" : "1")};
			transform: ${({ open }) => (open ? "translateY(10px)" : "translateY(0)")};
		}

		:nth-child(3) {
			transform: ${({ open }) => (open ? "rotate(-45deg)" : "rotate(0)")};
		}
	}
`;

interface IBurgerProps {
	open: boolean;
	setOpen: Dispatch<SetStateAction<boolean>>;
}

export default function Burger(props: IBurgerProps) {
	const { open, setOpen } = props;
	return (
		<StyledBurger open={open} onClick={() => setOpen(!open)}>
			<div />
			<div />
			<div />
		</StyledBurger>
	);
}

import "./assests/css/custom.css";
import "font-awesome/css/font-awesome.min.css";

import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// pages
import HomePage from "./pages/HomePage";
import PageNotFound from "./pages/PageNotFound";

const AppInner = () => {
	return (
		<>
			<Routes>
				<Route path="/" element={<HomePage />} />
				<Route path="*" element={<PageNotFound />} />
			</Routes>
		</>
	);
};

ReactDOM.render(
	<React.StrictMode>
		<Router>
			<AppInner />
		</Router>
	</React.StrictMode>,
	document.getElementById("root")
);

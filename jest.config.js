module.exports = {
	preset: "ts-jest",
	transform: {
		"^.+\\.tsx?$": "ts-jest",
	},
	collectCoverage: true,
	moduleNameMapper: {
		"\\.(svg)$": "<rootDir>/__mocks__/svgMock.js",
		"\\.(css)$": "<rootDir>/__mocks__/cssMock.js",
	},
	testEnvironment: "jsdom",
	testMatch: ["**/__tests__/**/*.[jt]s?(x)"],
};
